#!/bin/bash
LOCALE=$1
TYPE=numbers
EXTRACT_PARTS=(symbols decimalFormats percentFormats)
PACKAGE_NAME=@xcldr/data.$TYPE.$LOCALE
VERSION=0.0.1
GIT_URL=https://gitlab.com/earths_last_hope/cldr

if [ -z "$LOCALE" ]
then
    echo "No locale passed as argument"
    exit 1
fi

echo "Building for locale $LOCALE"

# Create dist folder if not exists
[ ! -d "dist" ] && mkdir dist

# If existing locale dir exists remove it
[ -d "dist/$LOCALE" ] && rm -rf dist/$LOCALE

# Create dist dir for the locale
mkdir dist/$LOCALE

# Extracting parts data into JSON files
for PART in ${EXTRACT_PARTS[@]}; do
    cat ../data-extract/common/main/$LOCALE.xml | xq .ldml.numbers.$PART > dist/$LOCALE/$PART.json
done

# Generate index.js file which exports all of the parts
for PART in ${EXTRACT_PARTS[@]}; do
echo "import $PART from './$PART.json';" >> dist/$LOCALE/index.js
done

echo "" >> dist/$LOCALE/index.js
echo "const meta = {" >> dist/$LOCALE/index.js
echo "  type: '$TYPE'," >> dist/$LOCALE/index.js
echo "  locale: '$LOCALE'," >> dist/$LOCALE/index.js
echo "};" >> dist/$LOCALE/index.js

echo "" >> dist/$LOCALE/index.js
echo "export {" >> dist/$LOCALE/index.js
for PART in ${EXTRACT_PARTS[@]}; do
echo "    $PART," >> dist/$LOCALE/index.js
done
echo "    meta" >> dist/$LOCALE/index.js
echo "}" >> dist/$LOCALE/index.js

echo "Done"

# Create package.json
echo "{" >> dist/$LOCALE/package.json
echo "  \"name\": \"$PACKAGE_NAME\"," >> dist/$LOCALE/package.json
echo "  \"version\": \"$VERSION\"," >> dist/$LOCALE/package.json
echo "  \"main\": \"transpiled.js\"," >> dist/$LOCALE/package.json
echo "  \"files\": [\"transpiled.js\"]," >> dist/$LOCALE/package.json
echo "  \"license\": \"MIT\"," >> dist/$LOCALE/package.json
echo "  \"scripts\": {" >> dist/$LOCALE/package.json
echo "    \"build\": \"babel index.js -o transpiled.js\"" >> dist/$LOCALE/package.json
echo "  }," >> dist/$LOCALE/package.json
echo "  \"repository\": {" >> dist/$LOCALE/package.json
echo "    \"url\": \"$GIT_URL\"," >> dist/$LOCALE/package.json
echo "    \"type\": \"git\"" >> dist/$LOCALE/package.json
echo "  }," >> dist/$LOCALE/package.json
echo "  \"devDependencies\": {" >> dist/$LOCALE/package.json
echo "    \"@babel/cli\": \"^7.10.5\"," >> dist/$LOCALE/package.json
echo "    \"@babel/core\": \"^7.10.5\"," >> dist/$LOCALE/package.json
echo "    \"@babel/preset-env\": \"^7.10.4\"," >> dist/$LOCALE/package.json
echo "    \"babel-plugin-inline-json-import\": \"^0.3.2\"" >> dist/$LOCALE/package.json
echo "  }," >> dist/$LOCALE/package.json
echo "  \"babel\": {" >> dist/$LOCALE/package.json
echo "    \"presets\": [\"@babel/preset-env\"]," >> dist/$LOCALE/package.json
echo "    \"plugins\": [\"inline-json-import\"]" >> dist/$LOCALE/package.json
echo "  }" >> dist/$LOCALE/package.json
echo "}" >> dist/$LOCALE/package.json

# Enter the build directory
cd dist/$LOCALE

# Install dependencies
yarn install

# Build transpiled package entry file
yarn build

# Create NPM package
yarn pack