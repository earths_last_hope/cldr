#!/bin/bash
source .env
ZIP_FILENAME=cldr-common.zip
curl -o $ZIP_FILENAME $DATASOURCE
unzip $ZIP_FILENAME -d ./data-extract
rm -rf $ZIP_FILENAME