# Building a data project

## Dependencies

Make sure you have the following installed on the computer that you're building on.

- yarn
- jq
- [yq](https://github.com/kislyuk/yq)

## How to build for a specific locale

If you haven't done so already you need to download the data extract before you can build any of the data packages.

```sh
./download-extract.sh
```

Navigate to the build project for the type that you wish to build a package for (e.g. build-numbers for the numbers type).

Run the following command.

```sh
./build.sh [locale_name]

Example:
./build.sh en
```