import { formatDecimal } from '@xcldr/core/numbers';
import * as numbersEn from '@xcldr/data.numbers.en';

const formatAction = (value) => {
    const results = formatDecimal(value, {
        locale: 'en'
    });

    console.log(`Formatting decimal value ${value}`);
    console.log(`Results: ${results}`);
    console.log();
}

formatAction(1337);
formatAction(50.25);
formatAction(300.7991);