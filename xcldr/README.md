# xcldr
Group of libraries to provide extract of CLDR data for a given locale and the type of data.

## Project statement
I would like for there to be a modular way to be able to include CLDR packages in your NodeJS projects where you only include the locales and types of data which you are interested in. This is to ensure that your build size is kept as small as possible. Also there would have to be modular packages for using the type of data that you want to have in your project.

I'm deciding on a namespacing system to consistently import the locales and types of data that you want in your project. For it to work we need to have a build process that automatically packages the CLDR data into multiple packages per data type and locale.

## Current status

No version released. Still elaborating the project and gathering my thoughts. No date set for any release or milestones at this point. 

But I do have a [proof of concept package builder](./data) for number data types. It's been designed so that it can build a numbers package for a given locale name and could be executed in a batch as part of a build automation process. NPM tarball packs are generated which can be installed locally. If we are satisfied with this approach then we could move on to publishing these directly to the [NPM Registry](http://npmjs.com).

## Brainstorming notes

The project is going to consist of multiple packages with only few of them maintained by human authors. Such packages are those that provide helper functionality for CLDR data access and the build of specific packages for specific locales and data types.

The work involved in builder packages is to grab data extracts from the [CLDR Repository](http://cldr.unicode.org/) and transform them into a structure that plays nicely with the new EcmaScript standard for both Web and NodeJS. The built packages should all get published to the NPM registry and when a consumer wants to include xcldr in their project they are required to install whichever package they need for the locales and the datatypes that they want to support.

### Package namespacing

Right now I'm thinking of how I want to namespace our packages. The first part of the namespace is always going to be @xcldr to denote the high level project name. After that we have sub-branches of packages by functionality.

**@xcldr/core** could be a package which has to be always installed. It contains various helper functions like how to format a currency amount in USD for the locale en-US. Or how to format a date string in fr-CA. As mentioned before a prerequisite to use such features would be to have the right data packages installed otherwise an error would be thrown.

It would be possible to branch out this package and avoid including the whole shebang of helper functions for every project. But for the sake of simplicity I would much rather just have one package. No matter how complex it will get it will not exceed more than a few megabytes in size and most build processes can handle treeshaking.

**@xcldr/data.type}.{locale}** is what I suggest now as the namespace for installing packages which include extracted data from [CLDR](http://cldr.unicode.org/). So for example **@xcldr/data.currencies.en_CA** would be the name of the package you must include if you were to format currencies in the en-CA locale.

### How will @xcldr/core read CLDR data?

One idea that I'm working towards is to involve a datasource singleton implementation. DataSource is a class which holds registrations of any data that is imported and added by the client before @xlcdr/core functionality can be used.
I added the meta object to the exports for the data builder project which indicates the locale and the type of the data that is pulled in.
Here is a demonstration of how the client could load in data ahead of time which gets included in his build.

```js
import { registerData } from '@xcldr/core';
import { formatDecimal } from '@xcldr/core/numbers';
import * as dataNumbersEn from '@xcldr/data.numbers.en';

registerData(dataNumbersEn);

// And now something like below should work
formatDecimal(1337, {
    locale: 'en'
});
```