import dataSource from './datasource';

const formatDecimal = (decimalValue, options) => {
    dataSource.listData();
    // TODO: Grab info from options to select
    // the culture from the request and also
    // the type of decimal format. Three different
    // decimal type formats can be supported which
    // are standard (default), long and short.
    return `${decimalValue}`;
}

export {
    formatDecimal
}