import dataSource from './datasource';

const { addData } = dataSource;

export {
    addData
}