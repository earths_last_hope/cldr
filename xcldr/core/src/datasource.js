class DataSource {
    #data = [];

    addData(data) {
        this.#data.push(data);
    }

    // this function is just for testing, will be removed in the future
    listData() {
        console.log(this.#data);
        return this.#data;
    }
}

const dataSource = new DataSource();
export default dataSource;