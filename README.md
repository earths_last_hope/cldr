# CLDR projects

I'm thinking of building a suite of projects to better serve CLDR features for NodeJS projects. 

Before any such project could be built I first want to have a solid data extract project to include specific types of data for specific locales alongside with helper functions. That project is now being established under the name [xcldr](./xcldr).

Once we have something basic in place we can move on to build interesting projects for React to render Currency inputs, datetime texts and even have integration with [react-i18next](https://github.com/i18next/react-i18next).

## Contribution

Get in touch with me if you have ideas about how we can improve things. As I'm just getting started there are a lot of things uncovered and if there is something of interest that you want to contribute to then I welcome you to. Otherwise, feel free to build any projects on your own on top of what I have built.